FROM ruby:2.4.4-alpine3.7
RUN mkdir app
COPY Gemfile* ./
ADD . app
WORKDIR app
RUN gem install bundler
RUN echo "install: --no-document" > ${HOME}/.gemrc
RUN echo "update: --no-document" >> ${HOME}/.gemrc
RUN apk update && \
    apk upgrade && \
    apk add --update --no-cache --virtual=.build-dependencies \
      sqlite-dev \
      build-base \
      curl-dev \
      linux-headers \
      libxml2-dev \
      libxslt-dev \
      ruby-dev \
      yaml-dev \
      tzdata \
      zlib-dev && \
    apk add --update --no-cache \
      sqlite-libs \
      sqlite \
      openntpd \
      git \
      openssh && \
    bundle install -j4 --without development test && \
    cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
    apk del .build-dependencies
RUN echo '*/3 * * * * cd /app && /usr/local/bundle/bin/bundle exec ruby /app/worker.rb >> /var/log/cron.log' > /var/spool/cron/crontabs/root
RUN bundle exec ruby /app/config/database.rb
RUN chmod 755 /app/shells/entry.sh
CMD ["/app/shells/entry.sh"]
