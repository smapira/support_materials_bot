# support_materials_bot

## usage
```bash
NAME_SPACE=support_materials_bot
CONTAINER=container-$(date +%Y-%m-%d)
docker build --rm -t ${NAME_SPACE}/${CONTAINER} .
docker run -d --name ${NAME_SPACE} \
    --privileged \
    --env-file .env \
    -v $(pwd)/log:/app/log:rw \
    -v $(pwd)/db:/app/db:rw \
    ${NAME_SPACE}/${CONTAINER}
```

## debug
```bash
docker exec -it ${NAME_SPACE} ash
docker run -it --name ${NAME_SPACE} ${NAME_SPACE}/${CONTAINER} ash
docker logs ${NAME_SPACE} 
docker stop ${NAME_SPACE} && docker rm ${NAME_SPACE}
docker stop ${NAME_SPACE} && docker start ${NAME_SPACE}
tail -f log/worker.log 
docker exec -t ${NAME_SPACE} ntpd -f /etc/ntpd.conf -s
docker exec -it ${NAME_SPACE} date
```