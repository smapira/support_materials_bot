#!/bin/sh

NAME_SPACE=support_materials_bot
CONTAINER=container-$(date +%Y-%m-%d)
docker stop ${NAME_SPACE} && docker rm ${NAME_SPACE}
docker build --rm -t ${NAME_SPACE}/${CONTAINER} .
docker run -d --name ${NAME_SPACE} \
    --privileged \
    -v $(pwd)/log:/app/log:rw \
    -v $(pwd)/db:/app/db:rw \
    ${NAME_SPACE}/${CONTAINER}
docker cp /Users/usr/Documents/Containers/support_materials_bot/.env ${NAME_SPACE}:/app/.
docker exec -t ${NAME_SPACE} ntpd -f /etc/ntpd.conf -s
docker exec -t ${NAME_SPACE} bundle exec ruby /app/config/database.rb

