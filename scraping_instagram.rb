# frozen_string_literal: true

require 'open-uri'
require 'json'

class ScrapingInstagram
  INSTAGRAM_URL = 'https://www.instagram.com/explore/tags/'
  INSTAGRAM_USER_URL = 'https://www.instagram.com/p/'

  def initialize(tag)
    @tag = tag
    @url = URI.encode("#{INSTAGRAM_URL}#{@tag}/?__a=1")
  end

  def request
    open(@url).read
  rescue Exception => e
    raise e
  end

  def take_photos
    content = JSON.parse(request, symbolize_names: true)
    photos = content[:graphql][:hashtag][:edge_hashtag_to_top_posts][:edges]
    photos.map do |x|
      { text: "instagram: #{INSTAGRAM_USER_URL}#{x[:node][:shortcode]}/?tagged=#{@tag} \n
      #{x[:node][:edge_media_to_caption][:edges][0][:node][:text]}".truncate(130),
        id: "ins_#{x[:node][:id]}",
        display_url: x[:node][:display_url] }
    end
  end
end
