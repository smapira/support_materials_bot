# frozen_string_literal: true

require 'active_record'

file_path = "#{File.dirname(__FILE__)}/../db/development.sqlite3"
ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: file_path
)
ActiveRecord::Migration.drop_table :tweets if ENV.fetch('INIT') { false }

if ENV.fetch('INIT') { false }
  ActiveRecord::Migration.create_table :tweets do |t|
    t.string  :tweet_id
    t.string  :user_id
    t.string  :screen_name
    t.text :status

    t.timestamps
    t.index :user_id
    t.index :screen_name
    t.index :tweet_id, unique: true
  end
end
if ENV.fetch('INIT') { false }
  if ENV.fetch('INIT') { false }
    ActiveRecord::Migration.add_column :tweets,
                                       :tags,
                                       :string,
                                       default: ''
  end
  ActiveRecord::Migration.add_column :tweets,
                                     :link,
                                     :integer,
                                     limit: 1,
                                     default: 0
end
