# frozen_string_literal: true

require 'rubygems'
require 'twitter'
require 'dotenv/load'
require 'logger'
require './tweet'
require './scraping_instagram'

class Worker
  def initialize(take_num_home, take_num_keyword, take_num_search)
    catch_rescue('assign_variables') { assign_variables }
    @take_num_home = take_num_home
    @take_num_keyword = take_num_keyword.to_i
    @take_num_search = take_num_search
    @client = Twitter::REST::Client.new do |config|
      config.consumer_key = ENV['CONSUMER_KEY']
      config.consumer_secret = ENV['CONSUMER_SECRET']
      config.access_token = ENV['ACCESS_TOKEN']
      config.access_token_secret = ENV['ACCESS_TOKEN_SECRET']
    end
  end

  def run
    catch_rescue('home_timeline') { retweet_timeline }
    catch_rescue('search') { search_tweets }
    return if !@tweets.is_a?(Array) || @tweets.empty?
    @tweets.map { |x| retweet(x, false) }
  end

  private

  def save_tweet(tweet, local)
    model = Tweet.new
    model.tweet_id = tweet.id
    model.user_id = tweet.user.id
    model.screen_name = tweet.user.screen_name
    model.status = tweet.text
    model.link = 1 unless local
    model.tags = tweet.attrs[:entities][:hashtags]
                      .map { |x| x[:text] }.sort!.join(', ')
    model.save
  end

  def assign_variables
    @tweets = []
    @logger = Logger.new("#{File.dirname(__FILE__)}/log/worker.log", 'daily')
    config = File.join(File.dirname(__FILE__), 'config/config.json')
    File.open(config, 'r:ASCII-8BIT') { |f| @options = JSON.parse(f.read) }
    @options = @options.each_with_object({}) { |(k, v), m| m[k.to_sym] = v; }
    @keywords = @options[:keyword].shuffle!.unshift('#避難勧告', '#避難指示')
  end

  def catch_rescue(name)
    yield
  rescue StandardError => e
    @logger.error "#{name}: #{e.message}"
    @logger.error e.backtrace.join("\n")
  end

  def ignore_tweet(tweet)
    return true if tweet.attrs[:retweeted]
    return true if include_words?(tweet.text, @options[:black_tag])
    return true unless include_words? tweet.text, @options[:white_tag]
    return true if Tweet.exists? tweet_id: tweet.id
    false
  end

  def follow(tweet, local)
    description = "#{tweet.user.name} #{tweet.user.description}"
    unless local && include_words?(description, @options[:black_tag])
      catch_rescue('follow') { @client.follow(tweet.user.id) }
    end
  end

  def retweet(tweet, local = true)
    return if ignore_tweet tweet
    catch_rescue('retweet') { @client.retweet(tweet.id) }
    @logger.info("#{tweet.user.id} #{tweet.user.screen_name} #{tweet.text}")
    save_tweet tweet, local
    follow tweet, local
  end

  def retweet_timeline
    @client.home_timeline(exclude_replies: true,
                          include_rts: false,
                          count: @take_num_home)
           .map { |x| retweet(x) }
  end

  def include_words?(tweet, words)
    words.any? { |x| tweet.downcase.include? x.downcase }
  end

  def search_tweets
    @keywords.take(@take_num_keyword).map do |x|
      @client.search(x, lang: 'ja', exclude: 'retweets',
                        count: @take_num_search,
                        since: Time.now.strftime('%Y-%m-%d'))
             .map { |tweet| @tweets << tweet }
    end
    tweet_instagram
    tweet_amazon
  end

  def tweet_instagram
    keyword = @options[:keyword].shuffle![0]
    photos = ScrapingInstagram.new(keyword.delete('#')).take_photos
    photos.each do |tweet|
      next if Tweet.exists? tweet_id: tweet[:id]
      img = open(tweet[:display_url])
      catch_rescue('tweet_instagram') { @client.update_with_media(tweet[:text], img) }
      @logger.info((tweet[:text]).to_s)
      save_tweet_instagram tweet
      break
    end
  end

  def tweet_amazon
    index = rand(0..500)
    return if index > 11
    @client.update @options[:offers][index]
  end

  def save_tweet_instagram(tweet)
    model = Tweet.new
    model.tweet_id = tweet[:id]
    model.status = tweet[:text]
    model.save
  end
end

Worker.new(50, ENV.fetch('TAKE_NUM_KEYWORD') { 15 }, 3).run
