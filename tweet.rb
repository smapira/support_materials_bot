# frozen_string_literal: true

require 'rubygems'
require 'active_record'

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: "#{File.dirname(__FILE__)}/db/development.sqlite3"
)

class Tweet < ActiveRecord::Base
  validates :tweet_id, uniqueness: true
  enum link: { local: 0, global: 1, error: 9 }, _suffix: true
end
